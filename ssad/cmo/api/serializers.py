import json

from rest_framework import serializers
from center.models import BattlePlan
from center.models import Casualties
from center.models import NineEleven
from center.models import ReportLogs


class JSONSerializerField(serializers.Field):
    """Serializer for JSONField -- required to make field writable"""

    def to_representation(self, value):
        json_data = {}
        try:
            json_data = json.loads(value)
        except ValueError as e:
            raise e
        finally:
            return json_data

    def to_internal_value(self, data):
        return json.dumps(data)


class BattlePlanSerializer(serializers.ModelSerializer):
    planning_data = JSONSerializerField()

    class Meta:
        model = BattlePlan

        fields = "__all__"


class CasualtiesSerializer(serializers.ModelSerializer):

    class Meta:
        model = Casualties
        fields = "__all__"

class NineElevenSerializer(serializers.ModelSerializer):

    class Meta:
        model = NineEleven
        fields = "__all__"

class ReportLogsSerializer(serializers.ModelSerializer):

    class Meta:
        model = ReportLogs
        fields = "__all__"


from django.conf.urls import url
from . import views
from rest_framework.urlpatterns import format_suffix_patterns

urlpatterns = [
    url(r'^battleplan/$', views.battleplan.as_view(), name='$battleplan'),
    url(r'^casualties/$', views.casualties.as_view(), name='$casualties'),
    url(r'^reports/$', views.nineelevenlog.as_view(), name='$reports'),
    url(r'^logs/$', views.reportLogs.as_view(), name='$logs')
]

urlpatterns = format_suffix_patterns(urlpatterns)


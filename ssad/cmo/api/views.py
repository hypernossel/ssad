from django.shortcuts import get_object_or_404
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from center.models import BattlePlan
from center.models import Casualties
from center.models import NineEleven
from center.models import ReportLogs
from .serializers import BattlePlanSerializer
from .serializers import CasualtiesSerializer
from .serializers import NineElevenSerializer
from .serializers import ReportLogsSerializer

import json


# Create your views here.
# BATTLE PLAN
class battleplan(APIView):

    def get(self, request):
        if request.GET.get('case_id'):
            return Response(BattlePlanSerializer(BattlePlan.objects.filter(case_id=request.GET.get('case_id')).first()).data)
        else:
            battleplans = BattlePlan.objects.all()
            serialize = BattlePlanSerializer(battleplans, many=True)
            return Response(serialize.data)

# Casualties
class casualties(APIView):

    def get(self, request):
        casualties = Casualties.objects.all()
        serialize = CasualtiesSerializer(casualties, many=True)
        return Response(serialize.data)

class nineelevenlog(APIView):

    def get(self, request):
        reports = NineEleven.objects.all()
        serialize = NineElevenSerializer(reports,many=True)
        return Response(serialize.data)

class reportLogs(APIView):

    def get(self, request):
        logs = ReportLogs.objects.all()
        serialize = ReportLogsSerializer(logs, many=True)
        return Response(serialize.data)

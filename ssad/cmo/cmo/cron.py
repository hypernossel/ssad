from django.conf import settings
from django.http import JsonResponse

from django_cron import CronJobBase, Schedule

import urllib.request, json

from center.models import BattlePlan
from center.models import Casualties

class UpdateDataFromPMO(CronJobBase):
    """
    Update Case Status from PMO
    """
    RUN_EVERY_MINS = 0 if settings.DEBUG else 1   # Every 1 Minute update

    schedule = Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'cron.UpdateDataFromPMO'

    def do(self):
        with urllib.request.urlopen("http://172.22.7.140:8000/status") as url:
            data = json.loads(url.read().decode())
            # Loop
            for i in range(0, len(data)):
                plan = BattlePlan.objects.get(case_id=data[i]['case_id'])
                print(plan.status + " " + data[i]['status'])
                if data[i]['dateauthorize'] is not None:
                    plan.status = data[i]['status']
                    plan.justification = data[i]['justification']
                    plan.date_auth = data[i]['dateauthorize']
                    plan.save()
                    print("saved " + plan.case_id)
                print("DONE")


class UpdateDataFromEF(CronJobBase):
    """
    Update Data From EF
    """

    def do(self):
        with urllib.request.urlopen("http://172.22.133.6/project/sendDataToCMO.php") as url:
            data = json.loads(url.read().decode())
            #Loop
            for i in range(0,len(data)):
                ef = Casualties.objects.get(case_id=data[i]['case_id'])
                if ef is None:
                    # Creating a new row
                    tmp = Casualties
                    tmp.case_id = data[i]['case_id']
                    tmp.injured = data[i]['casualties']
                    tmp.killed_in_action = data[i]['kia']
                    tmp.situation_status = data[i]['situation_status']
                    tmp.missing_in_action = data[i]['mia']
                    tmp.total_troops = data[i]['totalTroops']
                    tmp.save()
                else:
                    # Updating the row
                    ef.case_id = data[i]['case_id']
                    ef.injured = data[i]['casualties']
                    ef.killed_in_action = data[i]['kia']
                    ef.situation_status = data[i]['situation_status']
                    ef.missing_in_action = data[i]['mia']
                    ef.total_troops = data[i]['totalTroops']
                    ef.save()
            print("DONE")


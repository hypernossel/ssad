from django.contrib import admin
from .models import CMOUser

# Register your models here.
admin.site.register(CMOUser)

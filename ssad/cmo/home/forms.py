from django import forms


class LoginForm(forms.Form):
    user_id = forms.CharField(max_length=255)
    password = forms.CharField(max_length=255)

from django.http import HttpResponse
from django.shortcuts import render
from django.shortcuts import redirect

from django.contrib import messages

from .forms import LoginForm

from .models import CMOUser
from django.core import serializers

# Create your views here.
def index(request):
    return render(request, 'home/index.html',None)

# userasjson = serializers.serialize('json', CMOUser.objects.all())
# return HttpResponse(userasjson, content_type='json')
def _login(request):
    if request.method == 'POST':
        lgnfrm_data = LoginForm(request.POST)
        if lgnfrm_data.is_valid():
            user = lgnfrm_data.cleaned_data['user_id']
            pwd = lgnfrm_data.cleaned_data['password']
            udata = CMOUser.objects.filter(username=user, password=pwd)
            data = serializers.serialize('json', list(udata), fields=('uid', 'username', 'email', 'role'))
            if udata:
                request.session['profile'] = data
                return redirect('/center')
            else:
                messages.add_message(request, messages.ERROR, 'login information is incorrect.', extra_tags='alert-danger')
                return redirect('/home')
        else:
            messages.add_message(request, messages.ERROR, 'Oops something happened to the form.', extra_tags='alert-warning')
            return redirect('/home')
        # check stuff
    else:
        messages.add_message(request, messages.ERROR, 'Wrong form format??? admin please check', extra_tags='alert-warning')
        return redirect('/home')



from django.db import models
from django.core import serializers

#
# When Changing Database Models
# Execute these Commands
# - python manage.py makemigrations <Directory_NAME >
# - python manage.py migrate
#


# Create your models here.
class CMOUser(models.Model):
    # UID Primary Key
    uid = models.AutoField(primary_key=True)
    username = models.CharField(max_length=255)
    password = models.CharField(max_length=255)
    email = models.CharField(max_length=255)
    #Roles Types
    # 0 - Operator
    # 1 - General
    role = models.IntegerField()

    def __str__(self):
        return self.username

    def to_json(self):
        return dict(
            uid=self.uid, username=self.username,
            email=self.email,
            role=str(self.role))



import urllib

from django.core.serializers import json
from django.http import HttpResponse
from django.shortcuts import redirect
from django.shortcuts import render
import requests
# Import form template
from .forms import BattlePlanForm
from .forms import LogForm
from .forms import EOTForm

from .models import BattlePlan
from center.models import ReportLogs

from django.contrib import messages

from django.http import JsonResponse

# Create your views here.


def index(request):
    context = {"profile": request.session['profile']}
    return render(request, 'center/index.html', context)


def logout(request):
    del request.session['profile']
    return redirect('/home')


# URL For getting the form data and save
def save_battleplan(request):
    if request.method == "POST":
        form = BattlePlanForm(request.POST)
        if form.is_valid():
            BP = form.save(commit=False)
            form.save()
            return JsonResponse({"status": 200, "message": "BattlePlan Saved Successfully"})
        else:
            return JsonResponse({"status": 401, "message": "Error Saving BattlePlan See Logs"})
    else:
        return JsonResponse({"status": 401, "message": "Oops Invalid form data"})


def save_logs(request):
    if request.method == "POST":
        form = LogForm(request.POST)
        if form.is_valid():
            LF = form.save(commit=False)
            form.save()
            return JsonResponse({"status": 200, "message": "Logs Saved Successfully"})
        else:
            return JsonResponse({"status": 401, "message": "Error Saving Logs Something went wrong"})
    else:
        return JsonResponse({"status": 401, "message": "Oops Invalid form data"})


def remove_logs(request):
    if request.method == "POST":
        form = LogForm(request.POST)
        if form.is_valid():
            cid = request.POST.get("case_id", "")
            if cid is not None:
                ReportLogs.objects.filter(lid=int(cid)).delete()
                return JsonResponse({"status": 200, "message": "Logs have been deleted from existance"})
        else:
                return JsonResponse({"status": 401, "message": "Error deleting logs something went wrong"})
    else:
        return JsonResponse({"status": 401, "message": "Invalid Post"})

def update_pmo(request):
    try:
        with urllib.request.urlopen("http://172.22.7.140:8000/status") as url:
            data = json.loads(url.read().decode())
            # Loop
            for i in range(0, len(data)):
                plan = BattlePlan.objects.get(case_id=data[i]['case_id'])
                print(plan.status + " " + data[i]['status'])
                if data[i]['dateauthorize'] is not None:
                    plan.status = data[i]['status']
                    plan.justification = data[i]['justification']
                    plan.date_auth = data[i]['dateauthorize']
                    plan.save()
                    print("saved " + plan.case_id)
            return JsonResponse({"status": 200, "message": "DB have been updated"})
    except:
        return JsonResponse({"status": 401, "message": "Error Updating"})




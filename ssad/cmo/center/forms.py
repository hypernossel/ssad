from django import forms
from .models import BattlePlan
from .models import BattlePlanEOT
from .models import ReportLogs

class BattlePlanForm(forms.ModelForm):
    casetitle = forms.CharField(strip=False, max_length=255, widget=forms.Textarea)
    threatlevel = forms.IntegerField()
    affectedarea = forms.IntegerField()
    casedescription = forms.CharField(strip=False, max_length=255, widget=forms.Textarea)
    planning_data = forms.CharField(widget=forms.HiddenInput)

    class Meta:
        model = BattlePlan
        fields = ["casetitle", "threatlevel", "affectedarea", "casedescription", "planning_data"]


class EOTForm(forms.ModelForm):
    cid = forms.CharField(widget=forms.HiddenInput)
    EOT_plan = forms.CharField(max_length=255)

    class Meta:
        model = BattlePlanEOT
        fields = ["cid", "EOT_plan"]

class LogForm(forms.ModelForm):
    log_title = forms.CharField(strip=False, max_length=255, widget=forms.Textarea)
    log_content = forms.CharField(strip=False, max_length=255, widget=forms.Textarea)

    class Meta:
        model = ReportLogs
        fields = "__all__"
from django.db import models


# Create your models here.


class BattlePlan(models.Model):
    case_id = models.AutoField(primary_key=True)
    casetitle = models.CharField(max_length=255, default=None)
    casedescription = models.CharField(max_length=255, default=None)
    affectedarea = models.IntegerField(default=-1)
    threatlevel = models.IntegerField(default=-1)
    date_created = models.DateTimeField(auto_now=True, blank=True)
    planning_data = models.TextField(default=None)
    status = models.CharField(max_length=255, default="Pending")
    justification = models.CharField(max_length=255, default=None, blank=True, null= True)
    date_auth = models.DateTimeField(blank=True, null=True)

    def __str__(self):
        return self.casetitle + " -  status : " + str(self.affectedarea) + " Threat : " + str(self.threatlevel) + " Date Created :" + str(self.date_created)


class BattlePlanEOT(models.Model):
    cid = models.IntegerField()
    EOT_plan = models.CharField(max_length=255, default=None)

    def __str__(self):
        return "Plan ID : " + str(self.cid)

class Casualties(models.Model):
    case_id = models.IntegerField(default=-1)
    killed_in_action = models.IntegerField(default=0)
    missing_in_action = models.IntegerField(default=0)
    injured = models.IntegerField(default=0)
    total_troops = models.IntegerField(default=0)
    situation_status = models.TextField(default="", blank=None, null=True)

    def __str__(self):
        return str(self.case_id) + " : KIA - " + str(self.killed_in_action) + " MIA - " + str(self.missing_in_action) + " Injured - " + str(self.injured) + " Total Troops - " + str(self.total_troops)

class NineEleven(models.Model):
    c_rid = models.AutoField(primary_key=True)
    c_ruser = models.CharField(max_length=255, default="Anonymous")
    c_report_desc = models.CharField(max_length=255)
    c_report_location = models.CharField(max_length=255)

    def __str__(self):
        return "Report id : " + str(self.c_rid) + " loc: " + self.c_report_location

class ReportLogs(models.Model):
    lid = models.AutoField(primary_key=True)
    log_title = models.CharField(max_length=255)
    log_content = models.CharField(max_length=255)

    def __str__(self):
        return "Log " + str(self.lid) + " : " + self.log_title


from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^logout$', views.logout, name='logout'),
    url(r'^battleplan/save$', views.save_battleplan, name='save'),
    url(r'^log/save$', views.save_logs, name='save_logs'),
    url(r'^log/delete$', views.remove_logs, name='remove_logs'),
    url(r'^status/update$',views.update_pmo, name='update_pmo')
]


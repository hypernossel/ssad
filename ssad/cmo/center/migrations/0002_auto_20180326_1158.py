# Generated by Django 2.0.3 on 2018-03-26 11:58

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('center', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='battleplan',
            old_name='case_id',
            new_name='cid',
        ),
    ]

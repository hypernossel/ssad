# Generated by Django 2.0.3 on 2018-03-26 12:38

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('center', '0005_auto_20180326_1202'),
    ]

    operations = [
        migrations.AlterField(
            model_name='battleplan',
            name='threatlevel',
            field=models.IntegerField(default=-1),
        ),
    ]

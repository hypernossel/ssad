# Generated by Django 2.0.3 on 2018-03-26 12:01

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('center', '0003_auto_20180326_1158'),
    ]

    operations = [
        migrations.AlterField(
            model_name='battleplan',
            name='justification',
            field=models.CharField(blank=True, default=None, max_length=255),
        ),
    ]

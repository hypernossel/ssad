from django.contrib import admin
from .models import BattlePlan
from .models import NineEleven
from .models import Casualties
from .models import ReportLogs

# Register your models here.

admin.site.register(BattlePlan)
admin.site.register(NineEleven)
admin.site.register(Casualties)
admin.site.register(ReportLogs)
